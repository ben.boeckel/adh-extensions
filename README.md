ADH Extensions
==============

[Adaptive Hydraulics](https://www.erdc.usace.army.mil/Media/Fact-Sheets/Fact-Sheet-Article-View/Article/476708/adaptive-hydraulics-model-system/)
(ADH) is a modular, parallel, adaptive finite-element model for one-,
two- and three-dimensional flow and transport. ADH is a module of the
Department of Defense (DoD) Surface-Water Modeling System and
Ground-Water Modeling System. ADH simulates groundwater flow, internal
flow and open channel flow. The ADH module was developed in the
Engineer Research and Development Center’s Coastal and Hydraulics
Laboratory and is a product of the System-Wide Water Resources Program.

This project provides extensions for
[SMTK](https://www.computationalmodelbuilder.org/smtk/) to import,
export and manipulate data related to ADH.

License
=======

The ADH Extensions project is distributed under the OSI-approved BSD
3-clause License. See [License.txt][] for details.

[License.txt]: LICENSE.txt
