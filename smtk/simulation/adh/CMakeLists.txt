set(adhSrcs
  ExportBoundaryConditions.cxx
  ExportHotStartFile.cxx
)

set(adhHeaders
  ExportBoundaryConditions.h
  ExportHotStartFile.h
)

add_library(smtkADHSimulation ${adhSrcs})

target_include_directories(smtkADHSimulation PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:include>
)

set(moab_libs ${MOAB_LIBRARIES})
if(WIN32)
  set(moab_libs "MOAB")
endif()

target_link_libraries(smtkADHSimulation
  LINK_PUBLIC
    smtkCore
    smtkADHMesh
  LINK_PRIVATE
    Boost::boost
    Boost::filesystem
    ${moab_libs}
    pybind11::embed
)

install(
  TARGETS smtkADHSimulation
  EXPORT  ADHExtensions
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin)

smtk_export_header(smtkADHSimulation Exports.h)


set(adhPySrcs
  adhcommon.py
  operators/CTB.py
)

set(adh_pymodulefiles)

set(operatorsPyXML)
smtk_pyoperation_xml("${CMAKE_CURRENT_SOURCE_DIR}/operators/CTB.sbt" operatorsPyXML)

file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/smtksimulationadh)

foreach(pyxmlfile ${operatorsPyXML})
  get_filename_component(filename ${pyxmlfile} NAME)
  file(RENAME ${pyxmlfile} "${PROJECT_BINARY_DIR}/smtksimulationadh/${filename}")
  list(APPEND adh_pymodulefiles "${PROJECT_BINARY_DIR}/smtksimulationadh/${filename}")
endforeach()

foreach(pyfile ${adhPySrcs})
  get_filename_component(filename ${pyfile} NAME)
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/${pyfile}"
    "${PROJECT_BINARY_DIR}/smtksimulationadh/${filename}" @ONLY
    )
  list(APPEND adh_pymodulefiles "${PROJECT_BINARY_DIR}/smtksimulationadh/${filename}")
endforeach()
install(
  FILES ${adh_pymodulefiles}
  DESTINATION "${PYTHON_MODULEDIR}/smtksimulationadh"
  )

add_subdirectory(pybind11)
