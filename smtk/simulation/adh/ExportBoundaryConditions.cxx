//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================

#include "smtk/simulation/adh/ExportBoundaryConditions.h"

#include "smtk/mesh/adh/MeshPartition.h"

#include "smtk/attribute/Resource.h"

#include "smtk/common/PythonInterpreter.h"

#include "smtk/mesh/core/ForEachTypes.h"
#include "smtk/mesh/core/PointField.h"

#include "smtk/mesh/moab/Interface.h"

#include "smtk/simulation/UserData.h"

//force to use filesystem version 3
#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>

#include "moab/Core.hpp"
#include "moab/Interface.hpp"

#include <pybind11/embed.h>

#include <fstream>
#include <iomanip>

namespace
{

// Convert from SMTK cell type to AdH cell description string.
std::string to_CardType(smtk::mesh::CellType type)
{

  if (type == smtk::mesh::Line)
  {
    return std::string("E2L");
  }
  if (type == smtk::mesh::Triangle)
  {
    return std::string("E3T");
  }
  else if (type == smtk::mesh::Quad)
  {
    return std::string("E4Q");
  }
  else if (type == smtk::mesh::Tetrahedron)
  {
    return std::string("E4T");
  }
  else if (type == smtk::mesh::Pyramid)
  {
    return std::string("E5P");
  }
  else if (type == smtk::mesh::Wedge)
  {
    return std::string("E6W");
  }
  else if (type == smtk::mesh::Hexahedron)
  {
    return std::string("E8H");
  }
  return std::string("B4D");
}

// When writing out a triangle or quad region the cell must be written
// in counter clockwise orientation. We are presuming that for 2d meshes
// the triangles are all planar, so we can use the shoelace formula
// to determine if the points are in clockwise order.
// https://en.wikipedia.org/wiki/Shoelace_formula
double shoelace(const std::vector<double>& coordinates)
{
  double sum = 0;
  std::size_t nPoints = coordinates.size() / 3;
  for (std::size_t j = 0; j < nPoints; ++j)
  {
    std::size_t c1 = j;
    std::size_t c2 = ((j + 1) % nPoints);

    double x1 = coordinates[c1 * 3];
    double y1 = coordinates[c1 * 3 + 1];

    double x2 = coordinates[c2 * 3];
    double y2 = coordinates[c2 * 3 + 1];

    sum += (x2 - x1) * (y2 + y1);
  }
  return sum;
}

class IterateCells : public smtk::mesh::CellForEach
{
public:
  IterateCells(std::ofstream& meshGeometryFile,
               std::ofstream& boundaryConditionFile,
               const std::unordered_map<smtk::mesh::Handle, std::size_t>& pointMap,
               const smtk::mesh::HandleRange& faceAdjacentCells,
               const std::vector<smtk::mesh::adh::MeshPartition>& boundaryMeshes,
               const smtk::mesh::InterfacePtr& interface)
    : smtk::mesh::CellForEach()
    , m_meshGeometryFile(meshGeometryFile)
    , m_boundaryConditionFile(boundaryConditionFile)
    , m_pointMap(pointMap)
    , m_faceAdjacentCells(faceAdjacentCells)
    , m_boundaryMeshes(boundaryMeshes)
    , m_interface(std::dynamic_pointer_cast<smtk::mesh::moab::Interface>(
                    interface)->moabInterface())
    , m_cellIndex(1)
    , m_partitionIndex(1)
    {
    }

  void forCell(const smtk::mesh::Handle& cellId, smtk::mesh::CellType cellType,
               int numPts) override
    {
      // Write cell connectivity.
      {
        m_meshGeometryFile << to_CardType(cellType) << " \t " << m_cellIndex << " ";

        // when writing out a triangle or quad region the cell must be written
        // in counter clockwise orientation. We are presuming that for 2d meshes
        // the triangles are all planar, so we can use the shoelace formula
        // to determine if the points are in clockwise order.
        //  https://en.wikipedia.org/wiki/Shoelace_formula
        if ((cellType == smtk::mesh::Triangle || cellType == smtk::mesh::Quad) &&
            shoelace(coordinates()) > 0.)
        {
          for (int i = numPts - 1; i >= 0; --i)
          {
            m_meshGeometryFile << std::setw(8) << m_pointMap.at(pointId(i)) << " ";
          }
        }
        else
        {
          for (int i = 0; i < numPts; ++i)
          {
            m_meshGeometryFile << std::setw(8) << m_pointMap.at(pointId(i)) << " ";
          }
        }
        m_meshGeometryFile << std::setw(8) << m_partitionIndex << std::endl;
      }

      // Check if the cell is part of the boundary definition.
      if (smtk::mesh::rangeContains(m_faceAdjacentCells, cellId))
      {
        // If it is involved in the boundary definition, extract its
        // 2-dimensional adjacencies...
        std::vector<::moab::EntityHandle> adjacencies;
        m_interface->get_adjacencies(&cellId, 1, 2, false, adjacencies);

        // ...iterate each adjacency...
        for (auto& adjacency : adjacencies)
        {
          // ...determine to which boundary it belongs...
          for (std::size_t boundaryIndex = 1; boundaryIndex <= m_boundaryMeshes.size(); ++boundaryIndex)
          {
            if (smtk::mesh::rangeContains(m_boundaryMeshes[boundaryIndex - 1].cells().range(),
                                          adjacency))
            {
              // ...access the boundary cell's "side number" (the canonical
              // ordering information side number)...
              int sideNumber, sense, offset;
              m_interface->side_number(cellId, adjacency, sideNumber, sense, offset);

              // ...and write out the cell index, side number and boundary
              // index.
              m_boundaryConditionFile << "FCS" << std::setw(8) << m_cellIndex << " "
                                      << std::setw(8) << (sideNumber + 1) << " "
                                      << std::setw(8) << m_boundaryMeshes[boundaryIndex - 1].id()
                                      << std::endl;
            }
          }
        }
      }
      ++m_cellIndex;
    }

  void setPartitionIndex(std::size_t partitionIndex) { m_partitionIndex = partitionIndex; }

private:

  // Used to write out the geometry file
  std::ofstream& m_meshGeometryFile;
  std::size_t m_partitionIndex;

  // Used to write out the boundary file
  std::ofstream& m_boundaryConditionFile;
  const smtk::mesh::HandleRange& m_faceAdjacentCells;
  const std::vector<smtk::mesh::adh::MeshPartition>& m_boundaryMeshes;
  ::moab::Interface* m_interface;

  // Used for both
  const std::unordered_map<smtk::mesh::Handle, std::size_t>& m_pointMap;
  std::size_t m_cellIndex;
};

class IterateNodes : public smtk::mesh::CellForEach
{
public:
  IterateNodes(std::ofstream& boundaryConditionFile,
               const std::unordered_map<smtk::mesh::Handle, std::size_t>& pointMap)
    : smtk::mesh::CellForEach()
    , m_boundaryConditionFile(boundaryConditionFile)
    , m_pointMap(pointMap)
    , m_nodes()
    , m_partitionIndex(1)
    {
    }

  ~IterateNodes()
    {
      flush();
    }

  void forCell(const smtk::mesh::Handle& cellId, smtk::mesh::CellType cellType,
               int numPts) override
    {
      for (int i = 0; i < numPts; ++i)
      {
        m_nodes.insert(std::make_pair(m_pointMap.at(pointId(i)), m_partitionIndex));
      }
    }

  void setPartitionIndex(std::size_t partitionIndex) { m_partitionIndex = partitionIndex; }

private:

  void flush()
    {
      for (auto& node : m_nodes)
      {
        m_boundaryConditionFile << "NDS " << node.first << " " << node.second << std::endl;
      }

      m_nodes.clear();
    }

  std::ofstream& m_boundaryConditionFile;
  std::size_t m_partitionIndex;
  const std::unordered_map<smtk::mesh::Handle, std::size_t>& m_pointMap;
  std::set<std::pair<std::size_t, std::size_t>> m_nodes;
};

class IterateEdges : public smtk::mesh::CellForEach
{
public:
  IterateEdges(std::ofstream& boundaryConditionFile,
               const std::unordered_map<smtk::mesh::Handle, std::size_t>& pointMap)
    : smtk::mesh::CellForEach()
    , m_boundaryConditionFile(boundaryConditionFile)
    , m_pointMap(pointMap)
    , m_partitionIndex(1)
    {
    }

  void forCell(const smtk::mesh::Handle& cellId, smtk::mesh::CellType cellType,
               int numPts) override
    {
      m_boundaryConditionFile << "EGS "
                              << m_pointMap.at(pointId(0)) << " "
                              << m_pointMap.at(pointId(1)) << " "
                              << m_partitionIndex << std::endl;
    }

  void setPartitionIndex(std::size_t partitionIndex) { m_partitionIndex = partitionIndex; }

private:

  std::ofstream& m_boundaryConditionFile;
  std::size_t m_partitionIndex;
  const std::unordered_map<smtk::mesh::Handle, std::size_t>& m_pointMap;
};

template <typename Type>
Type getPyAttribute(pybind11::object& object, const std::string& name)
{
  if (!pybind11::hasattr(object, name.c_str()))
  {
    std::cerr << "could not find " << name << std::endl;
    return Type();
  }
  return object.attr(name.c_str()).cast<Type>();
}

}

namespace smtk
{
namespace simulation
{
namespace adh
{

void ExportBoundaryConditions::operator()(pybind11::object py_scope)
{
  // Access the attribute resource describing the simulation
  auto sim_atts = getPyAttribute<smtk::attribute::ResourcePtr>(py_scope, "sim_atts");

  // Access the model resource describing the input model
  auto model_resource = getPyAttribute<smtk::model::ResourcePtr>(py_scope, "model_resource");

  // Access the mesh resource describing the simulation mesh
  auto mesh_collection = getPyAttribute<smtk::mesh::ResourcePtr>(py_scope, "mesh_collection");

  // Compose the geometry and boundary condition ouput file names
  auto output_filebase = getPyAttribute<std::string>(py_scope, "output_filebase");
  auto output_directory = getPyAttribute<std::string>(py_scope, "output_directory");

  boost::filesystem::path geometryFile =
    boost::filesystem::path(output_directory) / output_filebase;
  geometryFile += ".3dm";

  boost::filesystem::path boundaryConditionFile =
    boost::filesystem::path(output_directory) / output_filebase;
  boundaryConditionFile += ".bc";

  return operator()(geometryFile.string(), boundaryConditionFile.string(), mesh_collection,
                    model_resource, "PartitionID", sim_atts);
}

void ExportBoundaryConditions::operator()(
  const std::string& geometryFileName,
  const std::string& boundaryConditionFileName,
  smtk::mesh::ResourcePtr& meshResource,
  smtk::model::ResourcePtr& modelResource,
  const std::string& modelPropertyName,
  smtk::attribute::ResourcePtr& simAtts)
{
  // Extract bc attributes by dimension
  std::set<smtk::attribute::AttributePtr> nodeBCs;
  std::set<smtk::attribute::AttributePtr> edgeBCs;
  std::set<smtk::attribute::AttributePtr> faceBCs;
  {
    std::size_t dimension = 3;
    auto meshset3D = meshResource->meshes(smtk::mesh::Dims3);
    if (meshset3D.is_empty())
    {
      dimension = 2;
    }

    auto bcAtts = simAtts->findAttributes("BoundaryCondition");

    std::set<std::size_t> nodeBCIds;
    std::set<std::size_t> edgeBCIds;
    std::set<std::size_t> faceBCIds;

    for (auto& bcAtt : bcAtts)
    {
      if (auto data = bcAtt->userData("BCID"))
      {
        std::size_t bcid = std::dynamic_pointer_cast<smtk::simulation::UserDataInt>(data)->value();
        if (bcAtt->definition()->isNodal())
        {
          // Only write the nodal string if it hasn't already been used
          if (nodeBCIds.find(bcid) == nodeBCIds.end())
          {
            nodeBCs.insert(bcAtt);
            nodeBCIds.insert(bcid);
          }
        }
        else if (dimension == 2)
        {
          // Only write the edge string if it hasn't already been used
          if (edgeBCIds.find(bcid) == edgeBCIds.end())
          {
            edgeBCs.insert(bcAtt);
            edgeBCIds.insert(bcid);
          }
        }
        else if (dimension == 3)
        {
          // Only write the face string if it hasn't already been used
          if (faceBCIds.find(bcid) == faceBCIds.end())
          {
            faceBCs.insert(bcAtt);
            faceBCIds.insert(bcid);
          }
        }
      }
    }
  }

  // Partition the mesh according to the model property name.
  std::vector<smtk::mesh::adh::MeshPartition> volumeMeshes =
    smtk::mesh::adh::partitionByModelProperty(
      meshResource, modelResource, modelPropertyName, smtk::mesh::Dims3);

  // Partition the surface mesh according to the boundary condition attributes.
  std::vector<smtk::mesh::adh::MeshPartition> boundaryMeshes =
    smtk::mesh::adh::partitionByAttributeAssociation(
      meshResource, faceBCs, smtk::mesh::Dims2);

  // Collect all of the points needed to describe the output mesh.

  std::size_t nCells, nPoints;

  // No default constructor! Give it dummy parameters and set it in the braced
  // logic below.
  smtk::mesh::PointSet points(meshResource, smtk::mesh::HandleRange());
  {
    smtk::mesh::CellSet cs = volumeMeshes[0].cells();
    const std::size_t size = volumeMeshes.size();
    for (std::size_t i = 1; i < size; ++i)
    {
      cs.append(volumeMeshes[i].cells());
    }
    points = cs.points();
    nCells = cs.size();
    nPoints = points.size();
  }

  // Construct a map connecting point handles to AdH's 1-based indexing. This is
  // usually done implicitly in smtk::mesh::extractTessellation; it is
  // repeatedly called when exporting region-partitioned meshes to XMS format.
  // By explicitly constructing this map once here, we avoid the overhead of
  // multiple point iterations.
  std::unordered_map<smtk::mesh::Handle, std::size_t> pointMap;
  {
    auto it = smtk::mesh::rangeElementsBegin(points.range());
    auto end = smtk::mesh::rangeElementsEnd(points.range());
    for (std::size_t counter = 1; it != end; ++it, ++counter)
    {
      pointMap[*it] = counter;
    }
  }

  // Collect all of the volume cells adjacent to the boundary surfaces. When we
  // loop over every volume cell, we will check if each cell is in this group
  // before performing the more expensive adjacency and canonical index
  // calculations.
  smtk::mesh::HandleRange faceAdjacentCells;
  {
    smtk::mesh::MeshSet ms = boundaryMeshes[0].m_meshSet;
    const std::size_t size = boundaryMeshes.size();
    for (std::size_t i = 1; i < size; ++i)
    {
      ms = smtk::mesh::set_union(ms, boundaryMeshes[i].m_meshSet);
    }
    meshResource->interface()->computeAdjacenciesOfDimension(
      ms.range(), smtk::mesh::Dims3, faceAdjacentCells);
  }

  // Construct a filestream for the geometry. The contents of this file are
  // generated entirely by this method, so we rewrite the file each time the
  // method is called.
  std::ofstream geometryFile(geometryFileName.c_str(), std::ios::out | std::fstream::trunc);

  writeGeometryPreamble(geometryFile, nCells, nPoints);

  writeBCPreamble(boundaryConditionFileName);

  // Construct a filestream for the boundary conditions. This file is written
  // by several methods across C++ and Python, so we append to the existing file
  // that was generated by "writeBCPreamble".
  std::ofstream bcFile(boundaryConditionFileName.c_str(), std::ios::out | std::fstream::app);

  writeNodeBoundaries(bcFile, meshResource, nodeBCs, pointMap);
  writeEdgeBoundaries(bcFile, meshResource, edgeBCs, pointMap);

  // Write face boundaries and geometry in one loop. This ensures that the index
  // for each volume element is correctly matched to the boundary face element
  // description.
  {
    IterateCells iterateCells(
      geometryFile, bcFile, pointMap, faceAdjacentCells, boundaryMeshes, meshResource->interface());

    for (auto& partition : volumeMeshes)
    {
      iterateCells.setPartitionIndex(partition.id());
      smtk::mesh::for_each(partition.cells(), iterateCells);
    }
  }

  // Finally, write the nodes to file.
  {
    double* coordinates = new double[3 * nPoints];
    points.get(coordinates);

    std::size_t counter = 0;
    for (int i = 0; i < nPoints; ++i, counter += 3)
    {
      geometryFile << "ND \t " << std::setw(8) << (i + 1) << " "
                   << std::fixed << std::setw(12) << coordinates[counter]
                   << " " << std::setw(12) << coordinates[counter + 1] << " "
                   << std::setw(12) << coordinates[counter + 2] << std::endl;
    }

    delete [] coordinates;
  }

  geometryFile.close();
  bcFile.close();
}

void ExportBoundaryConditions::writeGeometryPreamble(std::ofstream& geometryFile,
                                                     std::size_t nCells, std::size_t nPoints)
{
  // Fill in the file's preamble
  geometryFile << "MESH3D" << std::endl;
  geometryFile << "#NELEM " << nCells << std::endl;
  geometryFile << "#NNODE " << nPoints << std::endl;
}

void ExportBoundaryConditions::writeBCPreamble(const std::string& boundaryConditionFileName)
{
  using namespace pybind11::literals;
  auto locals = pybind11::dict("bc_path"_a=boundaryConditionFileName.c_str());

  // TODO: should we move the calls to write_MID_cards() and write_functions()
  // to this method? The benefit would be that this method would truly export
  // the boundary condition file from start to finish (with the exception of the
  // few lines at the beginning of the export operation). The functor would then
  // be useful in an entirely-C++ context, as opposed to its current state where
  // the generated file must be started in Python before this functor appends to
  // it.
  const char * bcPreamblePy = R"PYTHON(
from smtksimulationadh import adhcommon as adh
with open(bc_path, 'a+') as output:
    output.write('! entry point for ExportBoundaryConditions BC Preamble\n')
)PYTHON";

  pybind11::exec(bcPreamblePy, pybind11::globals(), locals);

}

void ExportBoundaryConditions::writeNodeBoundaries(
  std::ofstream& bcFile,
  smtk::mesh::ResourcePtr& meshResource,
  const std::set<smtk::attribute::AttributePtr>& nodeBCs,
  const std::unordered_map<smtk::mesh::Handle, std::size_t>& pointMap)
{
  // nodes may correspond to edges or faces, so we do not filter by dimension
  std::vector<smtk::mesh::adh::MeshPartition> meshPartitions =
    smtk::mesh::adh::partitionByAttributeAssociation(meshResource, nodeBCs);

  IterateNodes iterateNodes(bcFile, pointMap);

  for (auto& partition : meshPartitions)
  {
    iterateNodes.setPartitionIndex(partition.id());
    smtk::mesh::for_each(partition.cells(), iterateNodes);
  }
}

void ExportBoundaryConditions::writeEdgeBoundaries(
  std::ofstream& bcFile,
  smtk::mesh::ResourcePtr& meshResource,
  const std::set<smtk::attribute::AttributePtr>& edgeBCs,
  const std::unordered_map<smtk::mesh::Handle, std::size_t>& pointMap)
{
  std::vector<smtk::mesh::adh::MeshPartition> meshPartitions =
    smtk::mesh::adh::partitionByAttributeAssociation(meshResource, edgeBCs, smtk::mesh::Dims1);

  IterateEdges iterateEdges(bcFile, pointMap);

  for (auto& partition : meshPartitions)
  {
    iterateEdges.setPartitionIndex(partition.id());
    smtk::mesh::for_each(partition.cells(), iterateEdges);
  }
}

}
}
}
