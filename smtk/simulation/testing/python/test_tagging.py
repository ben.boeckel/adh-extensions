import os
import datetime as dt
import sys
import smtk
import smtk.attribute
import smtk.model
import smtk.io
import smtk.session.mesh
import smtk.simulation
import smtk.testing
import unittest

import smtksimulationadh


mesh_filename = "scenario.h5m"
att_filename  = "CTBV3_scenario.sbi"


#----------------------------------------------------------------------
class Tagging(smtk.testing.TestCase):

    def setUp(self):
        op = smtk.session.mesh.Import.create()
        fname = op.parameters().findFile('filename')
        fname.setValue(os.path.join(smtk.testing.DATA_DIR,mesh_filename))
        hierarchy = op.parameters().findVoid('construct hierarchy')
        hierarchy.setIsEnabled(True)
        res = op.operate()
        if res.findInt('outcome').value(0) != int(smtk.operation.Operation.SUCCEEDED):
            raise RuntimeError
        modelEntity = res.find('created').value(0)
        self.model = smtk.model.Model(
            modelEntity.modelResource(), modelEntity.id())
        self.model_resource = modelEntity.modelResource()
        self.mesh_resource = self.model_resource.meshTessellations()

    def testTagging(self):

        # access all volumes in the model
        volumes = self.model_resource.findEntitiesOfType(smtk.model.VOLUME)

        # there should only be two volumes (soil and target)
        assert len(volumes) == 2

        # select the volume with the fewest cells in its tessellation to be the
        # target. The other volume is the soil
        soil_volume = volumes[0]
        target_volume = volumes[1]
        if self.mesh_resource.findAssociatedMeshes(soil_volume).cells().size() < \
          self.mesh_resource.findAssociatedMeshes(target_volume).cells().size():
            tmp = soil_volume
            soil_volume = target_volume
            target_volume = tmp

        # Volume Groups

        # construct a group for the soil region
        soil_region_group = self.model_resource.addGroup(smtk.model.GROUP_3D, "soil_region")

        # add the target volume to the target region group
        soil_region_group.addEntity(soil_volume)

        # add partitionIDs as integers on the volumes for writing the 3dm
        soil_volume.setIntegerProperty("PartitionID", 1)

        # construct a group for the target region
        target_region_group = self.model_resource.addGroup(smtk.model.GROUP_3D, "target_region")

        # add the target volume to the target region group
        target_region_group.addEntity(target_volume)

        # add partitionIDs as integers on the volumes for writing the 3dm
        target_volume.setIntegerProperty("PartitionID", 2)

        # Face Groups

        # access all faces of the soil volume
        soil_faces = soil_volume.faces()

        # extract the bottom and top faces. They will have the smallest and
        # largest z_mid values (z_mid = average of the minimum and maximum z
        # extent components), respectively
        soil_bottom_face = soil_top_face = soil_faces[0]
        def z_mid(face):
            extent = smtk.mesh.extent(self.mesh_resource.findAssociatedMeshes(face))
            return .5 * (extent[4] + extent[5])

        z_mids = {}
        for soil_face in soil_faces:
            z_mids[soil_face] = z_mid(soil_face)

        for soil_face in soil_faces:
            if z_mids[soil_face] < z_mids[soil_bottom_face]:
                soil_bottom_face = soil_face
            if z_mids[soil_face] > z_mids[soil_top_face]:
                soil_top_face = soil_face

        # construct a group for the bottom soil face
        soil_bottom_face_group = self.model_resource.addGroup(smtk.model.GROUP_2D,
                                                              "soil_bottom_face")

        # add the soil bottom face to the soil bottom face group
        soil_bottom_face_group.addEntity(soil_bottom_face)

        # construct a group for the top soil face
        soil_top_face_group = self.model_resource.addGroup(smtk.model.GROUP_2D,
                                                              "soil_top_face")

        # add the soil top face to the soil top face group
        soil_top_face_group.addEntity(soil_top_face)

        # access all faces of the target volume
        target_faces = target_volume.faces()

        # extract the top face. It will have the largest z_mid value (z_mid =
        # average of the minimum and maximum z extent components)
        target_top_face = target_faces[0]
        z_mids = {}
        for target_face in target_faces:
            z_mids[target_face] = z_mid(target_face)

        for target_face in target_faces:
            if z_mids[target_face] > z_mids[target_top_face]:
                target_top_face = target_face

        # construct a group for the top target face
        top_surface_group = self.model_resource.addGroup(smtk.model.GROUP_2D,
                                                         "top_surface")

        # add the target top face to the target top face group
        top_surface_group.addEntity(target_top_face)

        ################Load sbt file##################
        # Load attribute file
        att_path = os.path.join(smtk.testing.DATA_DIR, att_filename)
        att_resource = smtk.attribute.Resource.create()
        reader = smtk.io.AttributeReader()
        logger = smtk.io.Logger()
        err = reader.read(att_resource, att_path, logger)
        if err:
            raise Exception('"load sbt" operator failed')

        ###########associate the model with the attributes##############
        att_resource.associate(self.model_resource)

        ###############Associate Materials with Groups of Volumes###########
        soil_mat = att_resource.findAttribute("Soil")
        soil_mat.associate(soil_region_group.component())

        target_mat = att_resource.findAttribute("Target")
        target_mat.associate(target_region_group.component())

        ################Associate boundary conditions with groups of faces########
        bottom_boundary_temp_bc = att_resource.findAttribute("BottomBoundaryTemp")
        bottom_boundary_temp_bc.associateEntity(soil_bottom_face_group)

        bottom_pressure_head_bc = att_resource.findAttribute("BottomPressure")
        success = bottom_pressure_head_bc.associateEntity(soil_bottom_face_group)

        ###changed the usemetdata bc to cMETData to handle the
        ########ADH order of operations issue (MET has to come before HFX)
        met_data_bc = att_resource.findAttribute("CMETData")
        met_data_bc.associateEntity(top_surface_group)

        gs_heat_flux_bc = att_resource.findAttribute("GroundSurfaceHeatFlux")
        gs_heat_flux_bc.associateEntity(top_surface_group)

        ###Modify the met file name for testing purposes by appending
        ######the data_path so that the writer will be able to find it
        gl = att_resource.findAttribute("Globals")
        metfile = gl.find("MetFileName")
        metfile.setValue(os.path.join(smtk.testing.DATA_DIR, metfile.value()))

        ################Add point fields for hotstart file#####################
        #####modify to use the first value in the boundary condition###########
        ms = self.mesh_resource.meshes(smtk.mesh.Dims3)
        #initial temperature
        it = []
        #set temperature to 15 for each node in mesh
        for i in range(self.mesh_resource.meshes().points().size()):
            it.append(30)
        self.mesh_resource.meshes().createPointField("IT", smtk.mesh.FieldType.Double, it)

        #initial total head
        ih = []
        #set initial total head to -20 for each node in mesh
        for i in range(self.mesh_resource.meshes().points().size()):
            ih.append(-20)
        self.mesh_resource.meshes().createPointField("IH", smtk.mesh.FieldType.Double, ih)
        ###################export bc file##############################

        solver_string = 'Groundwater Flow with Heat Transfer'
        categories = att_resource.analyses().find(solver_string).categories()
        print('Using categories: %s' % categories)

        export_ctb = smtksimulationadh.Export()

        #Find the analysis item and set the value
        analysis_item = export_ctb.parameters().find('AnalysisTypes')
        analysis_item.setValue(0, solver_string)

        #Find the model item and set the value to the capstone model
        model_item = export_ctb.parameters().find('model')
        model_item.setValue(0, self.model.component())

        #Find the mesh item and set the value to the mesh collection
        mesh_item = export_ctb.parameters().find('mesh')
        mesh_item.setValue(0,smtk.mesh.Component.create(self.mesh_resource.meshes()))

        #Find attributes mesh item and set the value to the simulation attributes
        attriburtes_item = export_ctb.parameters().find('attributes')
        attriburtes_item.setValue(0, att_resource)

        #Find the export mesh item and set to True so that the mesh is exported
        export_mesh_item = export_ctb.parameters().find("ExportMesh")
        export_mesh_item.setIsEnabled(True)

        #Find the Output directory and set the value to the data_path
        output_dir = os.path.join(smtk.testing.TEMP_DIR,
                                  dt.datetime.now().strftime("%Y-%m-%d_%H.%M.%S"))
        os.mkdir(output_dir)

        dir_item = export_ctb.parameters().find('OutputDirectory')
        dir_item.setValue(0, output_dir)

        ########set name in config file
        filebase_item = export_ctb.parameters().find('FileBase')
        filebase_item.setValue(0, "test")

        result = export_ctb.operate()
        if result.find('outcome').value(0) != int(smtk.operation.Operation.SUCCEEDED):
            print(export_ctb.log().convertToString())
            raise RuntimeError
        print("Finished")

if __name__ == '__main__':
    smtk.testing.process_arguments()
    unittest.main()
