//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/mesh/adh/operators/AnnotateCanonicalIndex.h"

#include <limits>

#include "smtk/mesh/adh/AnnotateCanonicalIndex_xml.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/IntItem.h"

#include "smtk/mesh/core/CellField.h"
#include "smtk/mesh/core/CellTraits.h"
#include "smtk/mesh/core/Component.h"
#include "smtk/mesh/core/Handle.h"
#include "smtk/mesh/core/MeshSet.h"
#include "smtk/mesh/core/Resource.h"

#include "smtk/mesh/moab/HandleRangeToRange.h"
#include "smtk/mesh/moab/Interface.h"

#include "moab/Core.hpp"
#include "moab/Interface.hpp"

namespace
{
// Return the dimension of the cell type
int dimension(smtk::mesh::CellType cellEnum)
{
  switch (cellEnum)
  {
    smtkMeshCellEnumToTypeMacro(return CellTraits::TOPOLOGICAL_DIMENSIONS);
    case smtk::mesh::CellType_MAX:
      break;
  }
  return -1;
}

class ExtractCanonicalIndex : public smtk::mesh::CellForEach
{
private:
  const smtk::mesh::Resource::Ptr& m_resource;
  smtk::mesh::CellField& m_canonicalIndexField;
  ::moab::Interface* m_interface;

public:
  ExtractCanonicalIndex(const smtk::mesh::Resource::Ptr& resource,
                         smtk::mesh::CellField& canonicalIndexField)
    : smtk::mesh::CellForEach(false)
    , m_resource(resource)
    , m_canonicalIndexField(canonicalIndexField)
    , m_interface(std::dynamic_pointer_cast<smtk::mesh::moab::Interface>(
                    resource->interface())->moabInterface())
  {
  }

  void forCell(const smtk::mesh::Handle& cellId, smtk::mesh::CellType type, int numPts) override
  {
    // Access the boundary cell's parent cell
    std::vector<::moab::EntityHandle> adjacencies;
    m_interface->get_adjacencies(&cellId, 1, dimension(type) + 1, false, adjacencies);

    // Exit early if the cell's parent was not found
    if (adjacencies.size() != 1)
    {
      std::cerr << "adjacency vector is length " << adjacencies.size() << " (should be 1)\n";
      return;
    }

    // Access the boundary cell's "side number" (the canonical ordering
    // information side number)
    int sideNumber, sense, offset;
    m_interface->side_number(adjacencies[0], cellId, sideNumber, sense, offset);

    // Assign the side number as cell data
    smtk::mesh::HandleRange range;
    range.insert(cellId);
    m_canonicalIndexField.set(range, &sideNumber);
  }
};


void annotateCanonicalIndex(smtk::mesh::MeshSet& mesh)
{
  // Construct a cell field on the meshset to hold the canonical index
  smtk::mesh::CellField canonicalIndexField =
    mesh.createCellField("canonical_index", 1, smtk::mesh::FieldType::Integer);

  // Populate the canonical index cell field
  auto resource = mesh.resource();
  ExtractCanonicalIndex extractCanonicalIndex(mesh.resource(), canonicalIndexField);
  smtk::mesh::for_each(mesh.cells(), extractCanonicalIndex);
}
}

namespace smtk
{
namespace mesh
{
namespace adh
{

bool AnnotateCanonicalIndex::ableToOperate()
{
  if (!this->Superclass::ableToOperate())
  {
    return false;
  }

  // Check that the underlying mesh interface is Moab (we use Moab's API
  // explicitly to extract side numbering).
  {
    smtk::attribute::ReferenceItem::Ptr meshItem = this->parameters()->associations();
    for (std::size_t i = 0; i < meshItem->numberOfValues(); i++)
    {
      smtk::mesh::Component::Ptr meshComponent = meshItem->valueAs<smtk::mesh::Component>(i);
      smtk::mesh::MeshSet mesh = meshComponent->mesh();

      if (mesh.resource()->interface()->name() != "moab")
      {
        return false;
      }
    }
  }

  return true;
}

AnnotateCanonicalIndex::Result AnnotateCanonicalIndex::operateInternal()
{
  // Access the meshes to annotate
  smtk::attribute::ReferenceItem::Ptr meshItem = this->parameters()->associations();

  // Construct a result object to hold our output
  Result result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);

  // Access the attribute associated with the modified model
  smtk::attribute::ComponentItem::Ptr modified = result->findComponent("modified");

  // Access the attribute associated with the changed tessellation
  auto modifiedEntities = result->findComponent("tess_changed");
  modifiedEntities->setNumberOfValues(meshItem->numberOfValues());

  // For each mesh component...
  for (std::size_t i = 0; i < meshItem->numberOfValues(); i++)
  {
    // ...access its underlying mesh..
    smtk::mesh::Component::Ptr meshComponent = meshItem->valueAs<smtk::mesh::Component>(i);
    smtk::mesh::MeshSet mesh = meshComponent->mesh();

    // ...and construct a cell field containing each cell's canonical index.
    annotateCanonicalIndex(mesh);

    // Add the annotated mesh to the output as modified.
    modified->appendValue(meshComponent);

    // Add the associated model entity to the output as modified.
    smtk::model::EntityRefArray entities;
    bool entitiesAreValid = mesh.modelEntities(entities);
    if (entitiesAreValid && !entities.empty())
    {
      smtk::model::Model model = entities[0].owningModel();
      modified->appendValue(model.component());
      modifiedEntities->appendValue(model.component());
    }
  }

  return result;
}

const char* AnnotateCanonicalIndex::xmlDescription() const
{
  return AnnotateCanonicalIndex_xml;
}
}
}
}
