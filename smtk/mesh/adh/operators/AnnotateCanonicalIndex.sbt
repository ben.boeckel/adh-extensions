<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the "annotate canonical index" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operation -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="annotate boundary conditions"
            Label="AdH - Annotate Canonical Index" BaseType="operation">
      <BriefDescription>
        Create a cell field on surface mesh elements that describes
        the cell's canonical index.
      </BriefDescription>
      <DetailedDescription>
        &lt;p&gt;Create a cell field on surface mesh elements that describes
        the cell's canonical index.
        &lt;p&gt;This operation constructs an integral
        cell field on the input surface mesh describing each element's
        "canonical index" as defined in
        &lt;p&gt;Tautges, Timothy J. "Canonical numbering systems for
        finite‐element codes." International Journal for Numerical
        Methods in Biomedical Engineering 26.12 (2010): 1559-1572.
      </DetailedDescription>

      <AssociationsDef Name="mesh" NumberOfRequiredValues="1" Extensible="true">
        <Accepts><Resource Name="smtk::mesh::Resource" Filter="meshset"/></Accepts>
      </AssociationsDef>

    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(annotate canonical index)" BaseType="result">
      <ItemDefinitions>
        <Component Name="tess_changed" NumberOfRequiredValues="0"
                   Extensible="true" AdvanceLevel="11">
          <Accepts><Resource Name="smtk::model::Resource" Filter=""/></Accepts>
        </Component>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
