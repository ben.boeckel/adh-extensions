//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_mesh_adh_operators_GenerateHotStartData_h
#define pybind_smtk_mesh_adh_operators_GenerateHotStartData_h

#include <pybind11/pybind11.h>

#include "smtk/mesh/adh/operators/GenerateHotStartData.h"

#include "smtk/operation/XMLOperation.h"

namespace py = pybind11;

PySharedPtrClass< smtk::mesh::adh::GenerateHotStartData, smtk::operation::XMLOperation > pybind11_init_smtk_adh_GenerateHotStartData(py::module &m)
{
  PySharedPtrClass< smtk::mesh::adh::GenerateHotStartData, smtk::operation::XMLOperation > instance(m, "GenerateHotStartData");
  instance
    .def(py::init<>())
    .def(py::init<::smtk::mesh::adh::GenerateHotStartData const &>())
    .def("deepcopy", (smtk::mesh::adh::GenerateHotStartData & (smtk::mesh::adh::GenerateHotStartData::*)(::smtk::mesh::adh::GenerateHotStartData const &)) &smtk::mesh::adh::GenerateHotStartData::operator=)
    .def_static("create", (std::shared_ptr<smtk::mesh::adh::GenerateHotStartData> (*)()) &smtk::mesh::adh::GenerateHotStartData::create)
    .def_static("create", (std::shared_ptr<smtk::mesh::adh::GenerateHotStartData> (*)(::std::shared_ptr<smtk::mesh::adh::GenerateHotStartData> &)) &smtk::mesh::adh::GenerateHotStartData::create, py::arg("ref"))
    .def("shared_from_this", (std::shared_ptr<const smtk::mesh::adh::GenerateHotStartData> (smtk::mesh::adh::GenerateHotStartData::*)() const) &smtk::mesh::adh::GenerateHotStartData::shared_from_this)
    .def("shared_from_this", (std::shared_ptr<smtk::mesh::adh::GenerateHotStartData> (smtk::mesh::adh::GenerateHotStartData::*)()) &smtk::mesh::adh::GenerateHotStartData::shared_from_this)
    ;
  return instance;
}

#endif
