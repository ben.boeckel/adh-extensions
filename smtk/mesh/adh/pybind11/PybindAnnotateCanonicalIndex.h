//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_mesh_adh_operators_AnnotateCanonicalIndex_h
#define pybind_smtk_mesh_adh_operators_AnnotateCanonicalIndex_h

#include <pybind11/pybind11.h>

#include "smtk/mesh/adh/operators/AnnotateCanonicalIndex.h"

#include "smtk/operation/XMLOperation.h"

namespace py = pybind11;

PySharedPtrClass< smtk::mesh::adh::AnnotateCanonicalIndex, smtk::operation::XMLOperation > pybind11_init_smtk_adh_AnnotateCanonicalIndex(py::module &m)
{
  PySharedPtrClass< smtk::mesh::adh::AnnotateCanonicalIndex, smtk::operation::XMLOperation > instance(m, "AnnotateCanonicalIndex");
  instance
    .def(py::init<>())
    .def(py::init<::smtk::mesh::adh::AnnotateCanonicalIndex const &>())
    .def("deepcopy", (smtk::mesh::adh::AnnotateCanonicalIndex & (smtk::mesh::adh::AnnotateCanonicalIndex::*)(::smtk::mesh::adh::AnnotateCanonicalIndex const &)) &smtk::mesh::adh::AnnotateCanonicalIndex::operator=)
    .def_static("create", (std::shared_ptr<smtk::mesh::adh::AnnotateCanonicalIndex> (*)()) &smtk::mesh::adh::AnnotateCanonicalIndex::create)
    .def_static("create", (std::shared_ptr<smtk::mesh::adh::AnnotateCanonicalIndex> (*)(::std::shared_ptr<smtk::mesh::adh::AnnotateCanonicalIndex> &)) &smtk::mesh::adh::AnnotateCanonicalIndex::create, py::arg("ref"))
    .def("shared_from_this", (std::shared_ptr<const smtk::mesh::adh::AnnotateCanonicalIndex> (smtk::mesh::adh::AnnotateCanonicalIndex::*)() const) &smtk::mesh::adh::AnnotateCanonicalIndex::shared_from_this)
    .def("shared_from_this", (std::shared_ptr<smtk::mesh::adh::AnnotateCanonicalIndex> (smtk::mesh::adh::AnnotateCanonicalIndex::*)()) &smtk::mesh::adh::AnnotateCanonicalIndex::shared_from_this)
    ;
  return instance;
}

#endif
