set(adhSrcs
  MeshPartition.cxx
  Registrar.cxx
  operators/AnnotateCanonicalIndex.cxx
  operators/GenerateHotStartData.cxx
)

set(adhHeaders
  MeshPartition.h
  Registrar.h
  operators/AnnotateCanonicalIndex.h
  operators/GenerateHotStartData.h
)

add_library(smtkADHMesh ${adhSrcs})

target_include_directories(smtkADHMesh PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:include>
)

set(moab_libs ${MOAB_LIBRARIES})
if(WIN32)
  set(moab_libs "MOAB")
endif()

target_link_libraries(smtkADHMesh
  LINK_PUBLIC
    smtkCore
  LINK_PRIVATE
    ${moab_libs}
)

install(
  TARGETS smtkADHMesh
  EXPORT  ADHExtensions
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin)

smtk_export_header(smtkADHMesh Exports.h)

smtk_operation_xml("${CMAKE_CURRENT_SOURCE_DIR}/operators/AnnotateCanonicalIndex.sbt"
  adhOperatorXML)
smtk_operation_xml("${CMAKE_CURRENT_SOURCE_DIR}/operators/GenerateHotStartData.sbt"
  adhOperatorXML)

add_subdirectory(pybind11)

if (TARGET smtkPluginSupport)

  include(SMTKPluginMacros)

  add_smtk_plugin(
    smtkADHMeshPlugin "1.0"
    REGISTRAR smtk::mesh::adh::Registrar
    REGISTRAR_HEADER smtk/mesh/adh/Registrar.h
    MANAGERS smtk::operation::Manager)

target_link_libraries(smtkADHMeshPlugin
  LINK_PUBLIC
  smtkCore
  smtkADHMesh
  )

install(
  TARGETS smtkADHMeshPlugin
  EXPORT  ADHExtensions
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin)

endif()
