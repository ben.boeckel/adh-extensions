add_subdirectory(adh)

if (ENABLE_TESTING)
  add_subdirectory(testing)
endif()
