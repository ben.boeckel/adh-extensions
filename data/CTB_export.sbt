<?xml version="1.0"?>
<SMTK_AttributeSystem Version="2">
  <Definitions>
    <AttDef Type="ExportSpec" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="model" Label="Model" Version="0" NumberOfRequiredValues="1">
        <MembershipMask>model</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="AnalysisTypes" Label="Analysis Types" AdvanceLevel="99" Version="0"
          Extensible="true" NumberOfRequiredValues="1"/>
        <MeshEntity Name="mesh" Label="Mesh" Version="0" NumberOfRequiredValues="1" />
        <Void Name="ExportMesh" Label="Export Mesh" Optional="true" IsEnabledByDefault="false"></Void>
        <Directory Name="OutputDirectory" Label="Output Directory" Version="1" NumberOfRequiredValues="1" />
        <String Name="FileBase" Label="FileName Base" Version="1" NumberOfRequiredValues="1">
          <BriefDescription>Common base name for output files (.2dm, .bc)</BriefDescription>
          <DefaultValue>surfacewater</DefaultValue>
        </String>
        <File Name="PythonScript" Label="Python script" AdvanceLevel="0" Version="0"  NumberOfRequiredValues="1"
              ShouldExist="true" FileFilters="Python files (*.py);;All files (*.*)">
          <DefaultValue>AdHSurfaceWater.py</DefaultValue>
        </File>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <Views>
    <View Type="Instanced" Title="Export Settings" TopLevel="true" FilterByCategory="false">
      <InstancedAttributes>
        <Att Name="Options" Type="ExportSpec" />
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeSystem>
